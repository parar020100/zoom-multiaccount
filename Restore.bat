@echo off
set /p login_name=Enter login name: %1
set login_name=%1%login_name%
if "%login_name%" == "" exit

call src\Kill.bat

if exist ".\data\%login_name%" (
	if exist "%APPDATA%\Zoom\data" rmdir /S /Q "%APPDATA%\Zoom\data"
	xcopy /I ".\data\%login_name%" %APPDATA%\Zoom\data
	rmdir /S /Q ".\data\%login_name%"
) else if exist "%APPDATA%\Zoom\data__%login_name%_backup" (
	if exist "%APPDATA%\Zoom\data" rmdir /S /Q "%APPDATA%\Zoom\data"
	xcopy /I %APPDATA%\Zoom\data__%login_name%_backup %APPDATA%\Zoom\data
	rmdir /S /Q "%APPDATA%\Zoom\data__%login_name%_backup"
)

del ".\Zoom (%login_name%).bat"
pause