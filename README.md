# Zoom Multiaccount
This allows to quickly launch zoom on windows with different accounts.

To add an account,  
	1. login with it and "do not exit" option and use **Add.bat**  
	2. enter any name of the account and click "enter"  

File **Zoom (<account_name>).bat** will appear in this directory.

***

This script works by creating multiple copies of %APPDATA%/Zoom/data directory
and replacing it with symbolic links to them.

A backup of data %APPDATA%/Zoom/*data_<account_name>_backup* is created for
every account. To restore the folder, use **Restore.bat** and type the account
name you used during installation.

***

Copyright 2020 Artiom Parygin (parar020100).

Any use of the scripts is allowed with a link to the source.

***