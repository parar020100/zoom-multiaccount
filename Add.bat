@echo off
set /p login_name=Enter login name: %1
set login_name=%1%login_name%
if "%login_name%" == "" exit
call src\Kill.bat
xcopy /I %APPDATA%\Zoom\data %APPDATA%\Zoom\data__%login_name%_backup
xcopy /I %APPDATA%\Zoom\data .\data\%login_name%
rmdir /S /Q "%APPDATA%\Zoom\data"
mklink /j "%APPDATA%\Zoom\data" ".\data\%login_name%"
echo @src\Launch.bat %login_name% >"Zoom (%login_name%).bat"
pause